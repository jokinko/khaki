package com.chmelar.khaku;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PushbuttonField;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.orm.SugarContext;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    private static final int CAMERA_REQUEST = 1888;
    private static final String TAG = "CHMELAR";
    private static final String PREFS_NAME = "MyPrefsFile";

    //bind views to variables
    @Bind(R.id.editTextAuditor)
    EditText txtAuditor;
    @Bind(R.id.editTextKlient)
    EditText txtKlient;
    @Bind(R.id.listViewNormy)
    ListView listViewNormy;
    @Bind(R.id.editTextNormy)
    AutoCompleteTextView txtNormy;

    private Norma norma;
    private ListAdapter normyAdapter;
    private List<Norma> listNorma = new ArrayList<>();

    /**
     *
     * @return random sentence
     */
    private String randomVeta() {
        List random = Arrays.asList("Cupcake ipsum dolor sit amet sugar plum Croissant candy canes dessert muffin I love bonbon pudding.g ummies chocolate bar ".split("\\s+"));
        String veta = "";
        Random r = new Random();
        for (int i = 0; i < r.nextInt(7); i++) {
            veta += " " + random.get(r.nextInt(random.size() - 1));
        }
        return veta;
    }

    /**
     * on the first run of app this method will add data to database
     */
    private void firstRun() {
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);

        if (settings.getBoolean("my_first_time", true)) {
            Toast.makeText(MainActivity.this, "Pridavam listNorma", Toast.LENGTH_LONG).show();
            Random r = new Random();
            for (int i = 1; i < 10; i++) {
                new Norma("Nedostatok " + (i + r.nextInt(20)), randomVeta()).save();
                new Norma("Chybajuci " + (i + r.nextInt(20)), randomVeta()).save();
                new Norma("Problem  ", randomVeta()).save();
            }
            Log.d("Comments", "First time");
            // first time task
            // record the fact that the app has been started at least once
            settings.edit().putBoolean("my_first_time", false).commit();
        }
    }

    @Override
    protected  void onResume( ){
     super.onResume();
        autoComplete();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        SugarContext.init(this);
        normyAdapter = new NormaAdapter(this, listNorma);
        listViewNormy.setAdapter(normyAdapter);
        authoraztionRun();
        firstRun();
        autoComplete(); //autocomplete for norm searching
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        //     Bitmap source = BitmapFactory.decodeResource(getResources(),getPath, options)


        try {
            if (getIntent() != null) {
                //get intent to add from VsetkyNormyActvitiy
                listNorma.add((Norma) getIntent().getExtras().get("novaNorma"));
                listViewNormy.setAdapter(normyAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            //get the photo from camera
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            photo = Bitmap.createScaledBitmap(photo, 200, 200, false);
            Norma n = norma;
            n.setObrazok(photo);
            listNorma.add(n);
            listViewNormy.setAdapter(normyAdapter);
        } catch (NullPointerException nullpointer) {
            Toast.makeText(MainActivity.this, "Chyba vkladania fotky", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //clics on items in menu
        switch (item.getItemId()) {
            case R.id.normy:
                startActivity(new Intent(this, VsetkyNormyActivity.class));
                return true;
            case R.id.done: // done button
                Toast.makeText(MainActivity.this, "done", Toast.LENGTH_SHORT).show();
                try {

                    createPDF();
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return true;
            default:
                super.onOptionsItemSelected(item);
        }
        return false;
    }

    /**
     * connects to chmelar.herokuapp.com/khaki and asks if it's permited to run app
     */
    private void authoraztionRun() {
        Ion i = Ion.getInstance(this, "");
        i.getCookieMiddleware().getCookieManager().getCookieStore().removeAll();
        try {
            Ion.with(this).load("GET", "https://chmelar.herokuapp.com/khaki/")
                    .asString()
                    .withResponse().setCallback(new FutureCallback<Response<String>>() {
                @Override
                public void onCompleted(Exception e, Response<String> result) {
                    if (result.getResult().equals("{\"Status\" :\"true\"}")) {
                        Toast.makeText(MainActivity.this, "autentifikovane", Toast.LENGTH_SHORT).show();
                    } else {
                        authorizationFail();
                    }
                }
            }).get();
        } catch (Exception e) {
            authorizationFail();
        }
    }

    //displays message if the app is not authorized to run
    private void authorizationFail() {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setTitle("Nedovolene spustenie")
                .setMessage("Pre spustenie aplikacie bez pristupu na internet kontaktujte prosim" +
                        " autora\n\n\njozefchmelar@gmail.com")
                .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                        dialog.cancel();

                    }
                }).show();
    }
    //endregion

    /**
     * assings adapter to textView so it can autocomplete data.
     */
    private void autoComplete() {
        //select * from Norma;
        final List<Norma> autocompleteNormy = Norma.listAll(Norma.class);
        //adapater for autocomplete
        final ArrayAdapter arrayAdapter = new ArrayAdapter<Norma>(this, android.R.layout.simple_list_item_1, autocompleteNormy);
        this.txtNormy.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                norma = (Norma) arrayAdapter.getItem(i);
                //clear text in form after zou click on it.
                txtNormy.setText("");
                //start camera
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });
        this.txtNormy.setAdapter(arrayAdapter);
    }

    /**
     * creates  folder for pdf
     * @return folder where we will store pdf
     */
    private File createPdfFolder() {
        Date date = new Date();
        String time = new SimpleDateFormat("dd-MM-yyyy").format(date);

        File pdfFolder = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).toString(),txtKlient.getText().toString());
        File pdfFolder2 = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS).toString()+"/"+txtKlient.getText().toString(),time);
        if (!pdfFolder.exists()) {
            pdfFolder.mkdir();
            Log.i(TAG, "Pdf Directory created");
        }
        if (!pdfFolder2.exists()) {
            pdfFolder2.mkdir();
            Log.i(TAG, "Pdf Directory created");
        }
        return pdfFolder;
    }

    /**
     *
     * This method will generate front page with the name of the creator,date and
     * costumer.
     * @throws DocumentException
     * @throws IOException
     */
    private void createPDF() throws DocumentException, IOException {
        //create file in DOWNLOADS directorz with the name of cleint
        File pdfFolder = createPdfFolder();
        //Create time stamp
        Date date = new Date();
        String time = new SimpleDateFormat("dd-MM-yyyy-ss ").format(new Date());
        String dateFolder = new SimpleDateFormat("dd-MM-yyyy").format(date);
        //Create file
        String pdfFilePath = pdfFolder+ "/"+dateFolder+ "/1 Titulna" + ".pdf";
        File myPdfFile = new File(pdfFilePath);
        //preapare stream for pdf writing
        OutputStream output = new FileOutputStream(myPdfFile);
        //Step 1
        Document document = new Document(PageSize.A4);
        PdfWriter writer = PdfWriter.getInstance(document, output);
        writer.setPageEmpty(false);
        //Step 3
        document.open();
        InputStream templateTitulna  = getAssets().open("titulna.pdf");
        //Load it into a reader
        PdfReader templateReaderTitulna  = new PdfReader(templateTitulna);
        //Get the page of the template
        PdfImportedPage pageTitulna = writer.getImportedPage(templateReaderTitulna, 1);
        //Now you can add it to you report
        PdfContentByte cb = writer.getDirectContent();
        document.newPage();
        cb.addTemplate(pageTitulna, 0, 0);
        PdfStamper stamperTitulna = new PdfStamper(templateReaderTitulna, output);
        //fields for front page
        AcroFields formsTitulna = stamperTitulna.getAcroFields();
        formsTitulna.setField("auditor", txtAuditor.getText().toString());
        formsTitulna.setField("klient", txtKlient.getText().toString());
        formsTitulna.setField("datum", time.substring(0, time.length() - 4).replace('-', '.'));
        //Step 5: Close the document
        formsTitulna.setGenerateAppearances(true);
        stamperTitulna.close();
        templateReaderTitulna.close();
        output.close();
        createNormPDf();
        archiveDir();
     //   Toast.makeText(MainActivity.this, myPdfFile.toURI().toString(), Toast.LENGTH_SHORT).show();
      //  sendMail(myPdfFile.toURI().toString(), "BOZP Konrola" + txtKlient.getText().toString() + "-" + time);
      //  Toast.makeText(MainActivity.this, "pdf done", Toast.LENGTH_SHORT).show();

    }

    /**
     * this method will go through all elements in list and generates specif pdf for every problem
     * that our user found.
     * @throws IOException
     * @throws DocumentException
     */
    private void createNormPDf() throws IOException, DocumentException {
        int count=2;
        for(Norma norma:listNorma){
            File pdfFolder = createPdfFolder();
            //Create time stamp
            Date date = new Date();
            String time = new SimpleDateFormat("dd-MM-yyyy").format(date);
            String dateFolder = new SimpleDateFormat("dd-MM-yyyy").format(date);
            //Create file
            String pdfFilePath = pdfFolder+ "/"+time+"/"+count++ +" Nedostatok" + ".pdf";
            File myPdfFile = new File(pdfFilePath);
            //preapare stream for pdf writing
            OutputStream output = new FileOutputStream(myPdfFile);
            //Step 1
            Document document = new Document(PageSize.A4);
            PdfWriter writer = PdfWriter.getInstance(document, output);
            writer.setPageEmpty(false);
            //Step 3
            document.open();
            InputStream templateTitulna  = getAssets().open("nedostatok.pdf");
            //Load it into a reader
            PdfReader templateReaderTitulna  = new PdfReader(templateTitulna);
            //Get the page of the template
            PdfImportedPage pageTitulna = writer.getImportedPage(templateReaderTitulna, 1);
            //Now you can add it to you report
            PdfContentByte cb = writer.getDirectContent();
            document.newPage();
            cb.addTemplate(pageTitulna, 0, 0);
            PdfStamper stamperTitulna = new PdfStamper(templateReaderTitulna, output);
            //fields for front page
            AcroFields formsTitulna = stamperTitulna.getAcroFields();
            formsTitulna.setField("textNedostatku", norma.toString());
            PushbuttonField ad = formsTitulna.getNewPushbuttonFromField("obrazokNedostatku");
            ad.setLayout(PushbuttonField.LAYOUT_ICON_ONLY);
            ad.setProportionalIcon(true);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            norma.getObrazok().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            ad.setImage(Image.getInstance(byteArray));
            formsTitulna.replacePushbuttonField("obrazokNedostatku", ad.getField());
            //Step 5: Close the document
            formsTitulna.setGenerateAppearances(true);
            stamperTitulna.close();
            templateReaderTitulna.close();
            output.close();
        }
    }

    /**
     * Sends mail
     * @param fileUri - Uri of file we want to send as attachment as stringg
     * @param subject- subject of email
     */
    private void sendMail(String fileUri, String subject) {
        Intent email = new Intent(android.content.Intent.ACTION_SEND);
        email.setType("application/octet-stream");
        email.putExtra(Intent.EXTRA_STREAM, Uri.parse(fileUri));
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"jozefchmelar@gmail.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, subject);
        startActivity(Intent.createChooser(email, "Send mail..."));
    }

    /**
     * creates Zip of our pdf files.
     */
    private void archiveDir() {
        try {
            Calendar calendar = Calendar.getInstance();
            Date time = calendar.getTime();
            long milliseconds = time.getTime();
            String path =Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS).toString()+"/"+txtKlient.getText()+"/";
            // Initiate ZipFile object with the path/name of the zip file.
            String stringTime = new SimpleDateFormat("dd-MM-yyyy-ss").format(new Date());

            ZipFile zipFile = new ZipFile(path+""+stringTime+".zip");

            // Folder to add
            String folderToAdd = createPdfFolder().toString();

            // Initiate Zip Parameters which define various properties such
            // as compression method, etc.
            ZipParameters parameters = new ZipParameters();

            // set compression method to store compression
            parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);

            // Set the compression level
            parameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);

            // Add folder to the zip file
            zipFile.addFolder(folderToAdd, parameters);
            sendMail(zipFile.getFile().toURI().toString(),zipFile.getFile().getName());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
