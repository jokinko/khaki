package com.chmelar.khaku;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;


/**
 * Created by Jozef on 1.5.2016.
 */
public class Norma extends SugarRecord {
    private String nazov;
    private String detail;
    private Bitmap obrazok;
    private String koment;

    //empty constructor for  orm
    public Norma() {
    }

    public Norma(String nazov, String detail) {
        this.nazov = nazov;
        this.detail = detail;
    }

    public Norma(String nazov, String detail, String koment, Bitmap obrazok) {
        this.nazov = nazov;
        this.detail = detail;
        this.koment = koment;
        this.obrazok = obrazok;
    }

    public String getKoment() {
        return koment;
    }

    public void setKoment(String koment) {
        this.koment = koment;
    }


    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setObrazok(Bitmap obrazok) {
        this.obrazok = obrazok;
    }

    public Bitmap getObrazok() {

        return obrazok;
    }

    /**
     *
     * @return Name and deatil of the norm as a string
     */
    @Override
    public String toString() {
        return nazov + " " + detail;
    }


}