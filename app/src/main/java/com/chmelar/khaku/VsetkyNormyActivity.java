package com.chmelar.khaku;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.orm.SugarContext;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VsetkyNormyActivity extends AppCompatActivity {

    @Bind(R.id.listViewVsetkyNormy)
    ListView listNormy;
    private ListAdapter normyAdapter;
    private List<Norma> normy;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vsetky_normy);
        ButterKnife.bind(this);
        SugarContext.init(this);
        normy  = Norma.listAll(Norma.class);
        normyAdapter = new NormaAdapter(this, normy);
        listNormy.setAdapter(normyAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.vsetky_normy_menu, menu);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.novaNorma:
                LayoutInflater linf = LayoutInflater.from(this);
                final View inflator = linf.inflate(R.layout.dialog_nova_norma, null);
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Nova norma");
                alert.setView(inflator);
                final EditText nazov = (EditText) inflator.findViewById(R.id.dialog_nazov);
                final EditText detail = (EditText) inflator.findViewById(R.id.dialog_detail);

                alert.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        String nazvoNormy = nazov.getText().toString();
                        String detailNormy = detail.getText().toString();
                        Toast.makeText(VsetkyNormyActivity.this, nazvoNormy, Toast.LENGTH_SHORT).show();
                        new Norma(nazvoNormy, detailNormy).save();

                    }
                });

                alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.cancel();
                    }
                });

                alert.show();

                return true;
            default:
                super.onOptionsItemSelected(item);
        }
        return false;
    }
}
