package com.chmelar.khaku;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Jozef on 1.5.2016.
 */
public class NormaAdapter extends ArrayAdapter<Norma> {
    private List<Norma> normy;
    private Context context;

    public NormaAdapter(Context context, List<Norma> normy) {
        super(context, R.layout.normy_adapter, normy);
        this.context = context;
        this.normy = normy;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.normy_adapter, parent, false);
        TextView nazov = (TextView) rowView.findViewById(R.id.textViewBig);
        TextView detail = (TextView) rowView.findViewById(R.id.textViewSmall);
        ImageView fotka = (ImageView) rowView.findViewById(R.id.imageViewObrazok);
        try {
            fotka.setImageBitmap(normy.get(position).getObrazok());

        if (normy.get(position).getObrazok() == null) {
            fotka.setVisibility(View.GONE);
        }} catch (Exception e) {
            e.printStackTrace();
        }
        //nazov.setT;
        nazov.setText(normy.get(position).getNazov());
        detail.setText(normy.get(position).getDetail());
        fotka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rowView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                normy.remove(position);
                                notifyDataSetChanged();
                                break;

                            case DialogInterface.BUTTON_NEGATIVE:                               //Do your No progress
                                break;
                            case DialogInterface.BUTTON_NEUTRAL:
                                Norma.delete(normy.get(position));
                                notifyDataSetChanged();
                                break;
                        }
                    }
                };
                AlertDialog ab = new AlertDialog.Builder(context)
                        .setMessage("Zmazat?")
                        .setNegativeButton ("Zmazat z databazy",dialogClickListener)
                        .setPositiveButton("Zmazat", dialogClickListener)
                        .setNeutralButton("Zrušiť", dialogClickListener)
                        .show();

                return false;
            }
        });
        return rowView;
    }


}
